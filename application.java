package supershop.shibigami.supershop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;

@SpringBootApplication
public class SupershopApplication {

	@CrossOrigin()
	public static void main(String[] args) {
		SpringApplication.run(SupershopApplication.class, args);
	}

}
