package supershop.shibigami.supershop.Model;

public class Product {
	String nome,descricao,idProduct;
	float price;
	
	
	
	public Product(String nome, String descricao, String idProduct, float price) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.idProduct = idProduct;
		this.price = price;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	public Product() {
		super();
	}
	
	
	

}
